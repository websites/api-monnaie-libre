import { FastifyInstance } from 'fastify'
import { IAppConfig } from '@/config/app'

declare module 'fastify' {
  export interface FastifyInstance {
    config: IAppConfig
  }
}
