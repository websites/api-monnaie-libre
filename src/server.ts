import fastify from '@/app'
import logger from '@/logger'

const app = fastify({
  logger,
  pluginTimeout: 50000,
  bodyLimit: 15485760
})

if (import.meta.env.PROD) {
  try {
    app.listen(app.config.PORT, '0.0.0.0')
    console.log(`Server started on 0.0.0.0:${app.config.PORT}`)
  } catch (err) {
    app.log.error(err)
    process.exit(1)
  }
}

export const viteNodeApp = app
