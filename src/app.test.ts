import { describe, it, expect, vi } from 'vitest'
import fastify from '@/test'

describe('Test server health', () => {
  it('serve GET /', async () => {
    const res = await fastify.inject('/')
    expect(res.json()).toEqual({ hello: 'monnaie libre !' })
  })

  it('serve swagger documentation', async () => {
    const res = await fastify.inject({
      method: 'GET',
      url: '/documentation'
    })
    expect(res.statusMessage).toEqual('Found')
  })
})
