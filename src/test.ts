import { beforeAll, afterAll } from 'vitest'

// Load .env file because vite prevent leaks, but we are in test mode. It's secure!
import dotenv from 'dotenv'
dotenv.config()

import buildFastify from './app'

const fastify = buildFastify()

beforeAll(async () => {
  // called once before all tests run
  await fastify.ready()
})
afterAll(async () => {
  // called once after all tests run
  await fastify.close()
})

export default fastify
