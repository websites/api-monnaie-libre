import { fastify as Fastify, FastifyServerOptions } from 'fastify'

import appConfig from '@/config/app'

import swagger from 'fastify-swagger'
import swaggerConfig from '@/config/swagger'

import gitlabWebhook from '@/plugins/gitlab-webhook'

export default (opts?: FastifyServerOptions) => {
  const fastify = Fastify(opts)

  // Load config from .env
  fastify.decorate('config', appConfig)

  // Load openAPI/Swagger
  fastify.register(swagger, swaggerConfig)

  fastify.register(gitlabWebhook)

  fastify.get('/', async (request, reply) => {
    return { hello: 'monnaie libre !' }
  })

  // Build openAPI/Swagger
  fastify.ready((err) => {
    if (err) throw err
    fastify.swagger()
  })

  return fastify
}
