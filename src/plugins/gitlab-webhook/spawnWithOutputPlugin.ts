import fp from 'fastify-plugin'

import { spawn } from 'child_process'
import { performance } from 'perf_hooks'

import fastifyWebsocket from 'fastify-websocket'
import { EventEmitter } from 'events'
import { readFileSync } from 'fs'

function spawnWithOutputPlugin(fastify, opts, done) {
  fastify.register(fastifyWebsocket, {
    handle(conn, req) {
      conn.pipe(conn) // creates an echo server
    },
    options: { maxPayload: 1048576 }
  })

  const SSEeventEmitter = new EventEmitter()

  // Handle `/output` routes both for http and websocket
  fastify.route({
    method: 'GET',
    url: '/output',
    handler: (req, reply) => {
      // this will handle http requests
      const page = readFileSync('src/plugins/gitlab-webhook/output.html')
      reply.header('Content-Type', 'text/html; charset=uft-8')
      reply.send(page)
    },
    wsHandler: (conn, req) => {
      // this will handle websockets connections
      conn.setEncoding('utf8')

      const id = req.query.id
      if (!id || id === 'null') {
        conn.socket.send(
          'Hello dev ! Set project ID with https://api.monnaie-libre.fr/output?id=ID'
        )
      } else {
        conn.socket.send(
          `Hello dev ! project_id: ${id}. Node version: ${process.version}\r\n\r\n`
        )

        SSEeventEmitter.on('data', (data) => {
          if (data.project_ID === id) conn.socket.send(data.msg)
        })
        SSEeventEmitter.on('close', (data) => {
          if (data.project_ID === id)
            conn.socket.send(
              `✨  Done in ${
                data.build_time_ms / 100
              }s.\r\n\r\n—————————————————————————\r\n\r\n`
            )
        })

        conn.once('data', (chunk) => {
          conn.end()
        })
      }
    }
  })

  function spawnWithOutput({ project_ID, command, args, onClose }) {
    const t0 = performance.now()

    const child = spawn(command, args, {
      // stdio: 'inherit'
    })

    child.stdout.setEncoding('utf8')
    child.stdout.on('data', (data) => {
      SSEeventEmitter.emit('data', { msg: data.toString(), project_ID })
    })
    child.stderr.on('data', (data) => {
      SSEeventEmitter.emit('data', { msg: data.toString(), project_ID })
    })

    child.on('close', () => {
      const build_time_ms = Math.round(performance.now() - t0)
      SSEeventEmitter.emit('close', { build_time_ms, project_ID })
      onClose(build_time_ms)
    })
  }

  fastify.decorate('spawnWithOutput', spawnWithOutput)

  done()
}

export default fp(spawnWithOutputPlugin, {
  name: 'spawnWithOutputPlugin'
})
