import envSchema from 'env-schema'
import { EventEmitter } from 'events'
import Pino from 'pino'
import spawnWithOutputPlugin from './spawnWithOutputPlugin'

function gitlabWebhook(fastify, opts, done) {
  // Load and check config from .env
  const config = envSchema({
    dotenv: true,
    schema: {
      type: 'object',
      required: ['GITLAB_WEBHOOK_SECRET_TOKEN'],
      properties: {
        GITLAB_WEBHOOK_SECRET_TOKEN: {
          type: 'string'
        }
      }
    }
  })

  fastify.decorate('gitlabWebhook', {
    // Add event emitter decorator
    emitter: new EventEmitter(),
    // Add gitlab webhook logger
    logger: Pino(
      {
        level: 'info',
        base: undefined // Remove pid and hostname. https://github.com/pinojs/pino/blob/HEAD/docs/api.md#base-object
      },
      Pino.destination('logs/gitlab-webhook.log')
    )
  })

  // Register tools
  fastify.register(spawnWithOutputPlugin)

  function verifyGitlabWebhook(request, reply, done) {
    // Verify token
    if (
      request.headers['x-gitlab-token'] !== config.GITLAB_WEBHOOK_SECRET_TOKEN
    ) {
      throw new Error('BAD gitlab webhook secret token')
    }

    // Verify message from gitlab
    if (!request.body?.object_kind) {
      throw new Error('BAD event message')
    }

    done()
  }

  /** Set POST route for gitlab webhook. Trigger an emit event with "object_kind" name
   * Possible object_kind:
   * - push
   * - issue
   * - note
   * - merge_request
   * - build (job event)
   * - pipeline
   **/
  fastify.post(
    '/gitlab-webhook',
    { preHandler: verifyGitlabWebhook },
    async (request, reply) => {
      // Log event
      fastify.gitlabWebhook.logger.info(request.body)
      // Emit event with object_kind as name of event
      fastify.gitlabWebhook.emitter.emit(request.body.object_kind, request)

      return 'done'
    }
  )

  // Events registering helper
  function registerSpawnFileAsEventName(event, request) {
    const file =
      process.env.NODE_ENV === 'production'
        ? `scripts/gitlab-webhook/${event}_event.sh`
        : `scripts/gitlab-webhook/${event}_event.sh.example`

    // Get project id
    const project_ID = request.body.project_id.toString()

    fastify.spawnWithOutput({
      project_ID,
      command: file,
      args: [project_ID],
      onClose(build_time_ms) {
        fastify.gitlabWebhook.logger.info({
          msg: `${event}_event`,
          project_ID,
          commit: request.body.after.slice(0, 8),
          build_time_ms
        })
      }
    })
  }

  // Register events
  fastify.gitlabWebhook.emitter.on('push', (request) =>
    registerSpawnFileAsEventName('push', request)
  )
  fastify.gitlabWebhook.emitter.on('tag_push', (request) =>
    registerSpawnFileAsEventName('tag_push', request)
  )

  done()
}

export default gitlabWebhook
