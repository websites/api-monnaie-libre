import { describe, expect, it } from 'vitest'
import fastify from '@/test'

import { InjectOptions } from 'fastify'
import { existsSync, unlinkSync } from 'fs'

import pushBody from './push-body.json'
import tagPushBody from './tag_push-body.json'

describe('gitlab webhook tests', () => {
  // Arrange
  const request: InjectOptions = {
    method: 'POST',
    url: '/gitlab-webhook'
  }

  it('throw bad gitlab token', async () => {
    // Act
    const res = await fastify.inject(request)
    // Assert
    expect(JSON.parse(res.body).message).toEqual(
      'BAD gitlab webhook secret token'
    )
  })

  it('throw bad event message', async () => {
    // Arrange
    request.headers = {
      'x-gitlab-token': process.env.GITLAB_WEBHOOK_SECRET_TOKEN as string
    }
    const res = await fastify.inject(request)
    expect(JSON.parse(res.body).message).toEqual('BAD event message')
  })

  it('trigger push event', async () => {
    // Arrange
    request.payload = pushBody
    // Act
    const res = await fastify.inject(request)
    // Assert
    expect(res.body).toEqual('done')
    // Wait for event emitter to perform task
    await new Promise((resolve) => setTimeout(resolve, 100))
    expect(existsSync('TEST_PUSH')).toBe(true)
    // Clean
    unlinkSync('TEST_PUSH')
  })

  it('trigger tag_push event', async () => {
    // Arrange
    request.payload = tagPushBody
    // Act
    const res = await fastify.inject(request)
    // Assert
    expect(res.body).toEqual('done')
    // Wait for event emitter to perform task
    await new Promise((resolve) => setTimeout(resolve, 100))
    expect(existsSync('TEST_TAG_PUSH')).toBe(true)
    // Clean
    unlinkSync('TEST_TAG_PUSH')
  })
})
