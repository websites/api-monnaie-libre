export default (fastify) => {
  return {
    routePrefix: '/documentation',
    swagger: {
      info: {
        title: 'API Documentation',
        description:
          'OpenAPI/Swagger API documentation for api.monnaie-libre.fr',
        version: '0.0.1'
      },
      externalDocs: {
        url: 'https://forum.monnaie-libre.fr',
        description: 'Find more info the forum! Ask for @ManUtopiK or @poka'
      },
      consumes: ['application/json'],
      produces: ['application/json']
    },
    exposeRoute: true
  }
}
