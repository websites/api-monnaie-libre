import envSchema from 'env-schema'

export interface IAppConfig {
  PORT: number
}

export default envSchema<IAppConfig>({
  dotenv: true,
  schema: {
    type: 'object',
    required: ['PORT'],
    properties: {
      PORT: {
        type: 'number',
        default: 3000
      }
    }
  }
})
