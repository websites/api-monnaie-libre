# API for monnaie-libre.fr

Started with https://github.com/ManUtopiK/vite-fastify-boilerplate

## What's this server does?

- Listen for gitlab webhook events and trigger some actions like update monnaie-libre.fr website
- Provide an API for custom frontend actions: new FAQ question or new ressource submit

## Config

Configure bash script in /scripts/gitlab-webhook.
Add the gitlab project number and script to perform some action from webhook.

## Install and use

Clone this repo or use [tiged](https://github.com/tiged/tiged) with:

```bash
degit https://git.duniter.org/websites/api-monnaie-libre.git api-monnaie-libre
```

**Start dev mode using Vite:**

```bash
pnpm dev
```

**Start test mode using Vitest:**

```bash
pnpm test
```

**Start test mode with coverage:**

```bash
pnpm coverage
```

**Compile typescript to javascript:**

```bash
pnpm build
```

**Start for production:**

```bash
pnpm start
```

## Code

- Main file: `src/app.ts`
- Configure logger: `src/logger.ts`
- Change port in `.env`

### Docs

- [Vite](https://vitejs.dev/)
- [Vite Plugin Node](https://github.com/axe-me/vite-plugin-node)
- [Vitest](https://vitest.dev/)
- [Fastify](https://www.fastify.io/docs/latest/)

### Ressources

- [Awesome vite](https://github.com/vitejs/awesome-vite)
- [Fastify ecosystem](https://www.fastify.io/ecosystem/)
